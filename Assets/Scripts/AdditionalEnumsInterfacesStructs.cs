﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalEnumsAndInterfaces : MonoBehaviour {

}
public enum WeaponTypes
{
    Pistol
}
public enum SkillTypes
{
    FastBuild,
    AttackHelp
}
public struct WeaponHelper
{
    public int Cost;
    public WeaponScript CurrentWeapon;
    public bool Opened;
    //еще сюды ресурсы добавить 
    public int ScrapNeed;
    public int Wood;
}
public class Skill
{
    public SkillTypes SkillType;
    public int Level;
}