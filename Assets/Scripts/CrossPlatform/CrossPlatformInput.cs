﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CrossPlatformInput {

    public static Dictionary<string, Vector2> JoySticks;
    static CrossPlatformInput()
    {
        JoySticks = new Dictionary<string, Vector2>();
    }
    public static void AddJoyStick(string id)
    {
        Vector2 catcher;
        if (!JoySticks.TryGetValue(id, out catcher))
            JoySticks.Add(id, Vector2.zero);
        else
            throw new System.Exception("Conflict joystick id");
    }
    public static Vector2 GetAxisJoystick(string id)
    {
        return JoySticks[id];
    }
    public static float GetXAxisJoystick(string id)
    {
        return JoySticks[id].x;
    }
    public static float GetYAxisJoystick(string id)
    {
        return JoySticks[id].y;
    }
    public static void SetJoyStickData(string id, Vector2 Data)
    {
        JoySticks[id] = Data;
    }
}
