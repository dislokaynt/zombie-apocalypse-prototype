﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MobileStickControl : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public int Range = 10;
    public string StickName;
    Vector2 _startPos;
    Transform _transform;
    // Use this for initialization
    void Start()
    {
        _transform = gameObject.transform;
        CrossPlatformInput.AddJoyStick(StickName);
        _startPos = new Vector2(_transform.position.x, _transform.position.y);
    }

    public void OnDrag(PointerEventData Data)
    {
        
        if (Vector2.Distance(_startPos, Data.position) < Range)
        {
            _transform.position = Data.position;
            CrossPlatformInput.SetJoyStickData(StickName, new Vector2((Data.position.x - _startPos.x) / Range, ( Data.position.y - _startPos.y) / Range));
        }
        else
        {
            float deltaX = Data.position.x - _startPos.x;
            float deltaY = Data.position.y - _startPos.y;
            Vector2 state = Vector2.ClampMagnitude(new Vector2(deltaX, deltaY), Range) ;
            _transform.position = state + _startPos;
            CrossPlatformInput.SetJoyStickData(StickName, new Vector2(state.x/Range,state.y/Range));
        }
    }
    public void OnEndDrag(PointerEventData Data)
    {
        CrossPlatformInput.SetJoyStickData(StickName, Vector2.zero);
        _transform.position = _startPos;
    }
}
