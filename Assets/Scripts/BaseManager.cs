﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseManager : MonoBehaviour {

    public List<SurvivorScript> Survivors;
    public WorkshopBuild Workshop;
    public WallBuild Wall;
    public TacticCenterBuild TacticCenter;
    public LivingSpaceBuild LivingSpace;
    public int WoodStorageMax;
    public int WoodCurrent;
    public int ScrapStorageMax;
    public int ScrapCurrent;
    // Use this for initialization
    private void Awake()
    {
        GameController.BaseManage = this;
        Workshop = new WorkshopBuild();
        Wall = new WallBuild();
        TacticCenter = new TacticCenterBuild();
        LivingSpace = new LivingSpaceBuild();
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
